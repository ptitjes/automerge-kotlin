pluginManagement {
    includeBuild("build-logic")

    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    @Suppress("UnstableApiUsage")
    repositories {
        mavenCentral()
    }
}

plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}

rootProject.name = "automerge-kotlin"

include(":libs:automerge-repository")
include(":libs:automerge-repository-websocket-ktor")
include(":libs:automerge-compose-ui")

include(":samples:compose-ui")
