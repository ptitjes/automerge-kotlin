import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import editors.MyObjectEditor
import models.MyObjectState
import models.createInitialBlankDocument
import org.automerge.compose.rememberDocumentRepository
import org.automerge.compose.rememberDocumentState
import org.automerge.repository.DocumentHandle
import org.automerge.repository.DocumentId
import org.automerge.repository.network.InProcessNetworkFactory

private val documentId = DocumentId.randomDocumentId()

fun main() = application {
    Window(onCloseRequest = ::exitApplication) {
        App()
    }
}

@Composable
fun App() {
    val coroutineScope = rememberCoroutineScope()
    val adapterFactory = remember { InProcessNetworkFactory(coroutineScope) }

    MaterialTheme(
        colors = if (isSystemInDarkTheme()) darkColors() else lightColors()
    ) {
        Surface(
            modifier = Modifier.fillMaxSize(),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth().padding(16.dp),
                horizontalArrangement = Arrangement.spacedBy(16.dp),
            ) {
                repeat(2) {
                    Editor(
                        modifier = Modifier.weight(1f),
                        adapterFactory = adapterFactory,
                    )
                }
            }
        }
    }
}

@Composable
fun Editor(
    adapterFactory: InProcessNetworkFactory,
    modifier: Modifier = Modifier,
) {
    val repository = rememberDocumentRepository(
        networkAdapters = listOf(adapterFactory.newAdapter()),
    )

    val handle = remember { repository.findOrCreate(documentId) { createInitialBlankDocument() } }

    val status by handle.statuses.collectAsState()

    status.let {
        when (it) {
            is DocumentHandle.Status.Ready -> {
                val documentState = rememberDocumentState(handle) { MyObjectState() }

                MyObjectEditor(
                    modifier = modifier,
                    myObjectState = documentState.root,
                )
            }

            else -> Row(
                modifier = modifier,
                horizontalArrangement = Arrangement.Center
            ) { CircularProgressIndicator() }
        }
    }
}
