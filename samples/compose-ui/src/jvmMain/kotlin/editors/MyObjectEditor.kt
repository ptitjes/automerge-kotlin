package editors

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Switch
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import models.MyObjectState

@Composable
fun MyObjectEditor(
    myObjectState: MyObjectState,
    modifier: Modifier = Modifier,
) {
    Card(modifier = modifier) {
        Column(
            modifier = Modifier.padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Switch(
                checked = myObjectState.someFlag,
                onCheckedChange = { myObjectState.someFlag = it }
            )

            TextField(
                modifier = Modifier.fillMaxWidth(),
                value = myObjectState.someString,
                onValueChange = { myObjectState.someString = it }
            )

            MySubObjectEditor(
                modifier = modifier,
                mySubObjectState = myObjectState.someSubObject
            )
        }
    }
}
