package models

import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import org.automerge.compose.AmObjectState

class MyObjectState : AmObjectState() {
    var someFlag by booleanProperty("someFlag")
    var someString by strProperty("someString")

    val someSubObject = objectProperty("someSubObject") { MySubObjectState() }
}
