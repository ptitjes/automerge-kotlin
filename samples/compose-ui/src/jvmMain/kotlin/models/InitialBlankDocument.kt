package models

import org.automerge.Document
import org.automerge.ObjectId
import org.automerge.ObjectType
import org.automerge.repository.createDocument

private val nullActorId = ByteArray(16) { 0 }

internal fun createInitialBlankDocument(): Document = createDocument(nullActorId) {
    set(ObjectId.ROOT, "someFlag", false)
    set(ObjectId.ROOT, "someString", "Some string")
    val subObjectId = set(ObjectId.ROOT, "someSubObject", ObjectType.MAP)

    set(subObjectId, "someOtherString", "Some other string")
}
