package models

import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import org.automerge.compose.AmObjectState

class MySubObjectState : AmObjectState() {
    var someOtherString by strProperty("someOtherString")
}
