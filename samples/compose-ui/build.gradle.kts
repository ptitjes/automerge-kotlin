plugins {
    id("automerge-compose")
}

kotlin {
    jvm()

    sourceSets {
        jvmMain {
            dependencies {
                implementation(libs.automerge.java)
                implementation(project(":libs:automerge-repository"))
                implementation(compose.desktop.currentOs)
                implementation(compose.material3)
                implementation(libs.kotlinx.datetime)

                implementation(project(":libs:automerge-compose-ui"))
            }
        }
    }
}
