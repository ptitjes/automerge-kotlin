package org.automerge.repository.network

import org.automerge.repository.DocumentId
import org.automerge.repository.PeerId

public sealed interface PeerMessage {
    public val senderId: PeerId
    public val targetId: PeerId
    public val documentId: DocumentId

    public data class Synchronization(
        override val senderId: PeerId,
        override val targetId: PeerId,
        override val documentId: DocumentId,
        val data: ByteArray,
    ) : PeerMessage {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Synchronization

            if (senderId != other.senderId) return false
            if (targetId != other.targetId) return false
            if (documentId != other.documentId) return false
            if (!data.contentEquals(other.data)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = senderId.hashCode()
            result = 31 * result + targetId.hashCode()
            result = 31 * result + documentId.hashCode()
            result = 31 * result + data.contentHashCode()
            return result
        }

        @OptIn(ExperimentalStdlibApi::class)
        override fun toString(): String {
            return "Synchronization(senderId=$senderId, targetId=$targetId, documentId=$documentId, data=${data.toHexString()})"
        }
    }

    public data class Ephemeral(
        override val senderId: PeerId,
        override val targetId: PeerId,
        override val documentId: DocumentId,
        val data: ByteArray,
    ) : PeerMessage {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Ephemeral

            if (senderId != other.senderId) return false
            if (targetId != other.targetId) return false
            if (documentId != other.documentId) return false
            if (!data.contentEquals(other.data)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = senderId.hashCode()
            result = 31 * result + targetId.hashCode()
            result = 31 * result + documentId.hashCode()
            result = 31 * result + data.contentHashCode()
            return result
        }

        @OptIn(ExperimentalStdlibApi::class)
        override fun toString(): String {
            return "Ephemeral(senderId=$senderId, targetId=$targetId, documentId=$documentId, data=${data.toHexString()})"
        }
    }
}
