package org.automerge.repository

public data class DocumentId(val uuid: ByteArray) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DocumentId

        return uuid.contentEquals(other.uuid)
    }

    override fun hashCode(): Int {
        return uuid.contentHashCode()
    }

    @OptIn(ExperimentalStdlibApi::class)
    override fun toString(): String = uuid.toHexString()

    public companion object {
        public fun randomDocumentId(): DocumentId = DocumentId(randomUUID())
    }
}

@OptIn(ExperimentalStdlibApi::class)
public fun String.toDocumentId(): DocumentId = DocumentId(hexToByteArray())
