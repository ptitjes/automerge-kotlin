package org.automerge.repository

import java.nio.ByteBuffer
import java.util.UUID

internal fun randomUUID(): ByteArray {
    val uuid = UUID.randomUUID()
    return ByteBuffer.allocate(16).putLong(uuid.mostSignificantBits).putLong(uuid.leastSignificantBits).array()
}
