package org.automerge.repository.network

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.automerge.repository.PeerId

public class InProcessNetworkFactory(
    private val coroutineScope: CoroutineScope,
) {

    private val _allPeerEvents = MutableSharedFlow<PeerEvent>()
    private val _allMessages = MutableSharedFlow<PeerMessage>()

    public fun newAdapter(): NetworkAdapter = Adapter()

    private inner class Adapter : NetworkAdapter {

        private val _peerEvents = MutableSharedFlow<PeerEvent>()
        private val _peerMessages = MutableSharedFlow<PeerMessage>()

        private var peerId: PeerId? = null
        private var adapterJob: Job? = null

        override suspend fun connect(peerId: PeerId) {
            adapterJob = coroutineScope.launch(start = CoroutineStart.UNDISPATCHED) {
                launch(start = CoroutineStart.UNDISPATCHED) {
                    _allPeerEvents.filter { it.peerId != peerId }
                        .onEach { event ->
                            launch {
                                if (event is PeerEvent.Join) {
                                    _allPeerEvents.emit(PeerEvent.Welcome(peerId))
                                }
                            }
                        }.collect(_peerEvents)
                }

                launch(start = CoroutineStart.UNDISPATCHED) {
                    _allMessages.filter { it.targetId == peerId }.collect(_peerMessages)
                }
            }

            this.peerId = peerId

            _allPeerEvents.emit(PeerEvent.Join(peerId))
        }

        override val peerEvents: Flow<PeerEvent> = _peerEvents.asSharedFlow()

        override suspend fun send(message: PeerMessage) {
            if (peerId == null) error("Network adapter is not connected")
            _allMessages.emit(message)
        }

        override val peerMessages: Flow<PeerMessage> = _peerMessages.asSharedFlow()
    }
}
