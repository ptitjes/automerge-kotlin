package org.automerge.repository

import org.automerge.Document
import org.automerge.Transaction

public fun createDocument(actorId: ByteArray? = null, block: Transaction.() -> Unit): Document {
    val document = if (actorId != null) Document(actorId) else Document()
    val tx = document.startTransaction()
    tx.block()
    tx.commit()
    return document
}
