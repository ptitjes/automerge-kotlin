package org.automerge.repository.network

import kotlinx.coroutines.flow.Flow
import org.automerge.repository.PeerId

public interface NetworkAdapter {
    public suspend fun connect(peerId: PeerId)
    public val peerEvents: Flow<PeerEvent>

    public suspend fun send(message: PeerMessage)
    public val peerMessages: Flow<PeerMessage>
}
