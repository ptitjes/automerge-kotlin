package org.automerge.repository

@JvmInline
public value class PeerId(public val id: String) {
    override fun toString(): String = id
}

public fun randomPeerId(): PeerId = PeerId("user-${Math.round(Math.random() * 100000)}")
