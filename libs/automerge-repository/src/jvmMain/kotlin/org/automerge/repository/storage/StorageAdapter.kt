package org.automerge.repository.storage

public typealias StorageKey = Array<String>

public interface StorageAdapter {
    public suspend fun load(key: StorageKey): ByteArray?
    public suspend fun save(key: StorageKey, data: ByteArray)
    public suspend fun remove(key: StorageKey)
}
