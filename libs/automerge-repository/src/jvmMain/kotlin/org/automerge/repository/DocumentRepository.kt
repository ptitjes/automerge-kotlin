package org.automerge.repository

import co.touchlab.kermit.Logger
import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.update
import kotlinx.atomicfu.updateAndGet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.automerge.Document
import org.automerge.repository.network.NetworkAdapter
import org.automerge.repository.network.NetworkSubsystem
import org.automerge.repository.network.PeerEvent
import org.automerge.repository.network.PeerMessage
import org.automerge.repository.storage.StorageAdapter
import org.automerge.repository.storage.StorageSubsystem
import kotlin.coroutines.CoroutineContext

public fun CoroutineScope.newDocumentRepository(
    networkAdapters: List<NetworkAdapter>,
    storageAdapter: StorageAdapter? = null,
    localPeerId: PeerId = randomPeerId(),
): DocumentRepository = DocumentRepository(this.coroutineContext, networkAdapters, storageAdapter, localPeerId)

public class DocumentRepository(
    coroutineContext: CoroutineContext,
    networkAdapters: List<NetworkAdapter>,
    storageAdapter: StorageAdapter?,
    private val localPeerId: PeerId,
) {
    private val coroutineScope = CoroutineScope(coroutineContext)

    private val log = Logger.withTag("${localPeerId.id}|DocumentRepository")

    private val storage = storageAdapter?.let { StorageSubsystem(it) }
    private val network = NetworkSubsystem(networkAdapters)

    private val allPeers = atomic(setOf<PeerId>())
    private val handles = atomic(mapOf<DocumentId, DocumentHandle>())

    private var repositoryJob: Job? = null

    init {
        repositoryJob = coroutineScope.launch {
            launch {
                log.i { "Listening to peer events" }
                network.peerEvents.collect { event ->
                    log.d { "Received peer event $event" }
                    when (event) {
                        is PeerEvent.Welcome -> addPeer(event.peerId)
//                        is PeerEvent.Disconnected -> removePeer(event.peerId)
                        else -> {}
                    }
                }
            }
            launch {
                log.i { "Listening to messages" }
                network.messages.collect { message ->
                    require(message.targetId == localPeerId)
                    log.d { "Received peer message $message" }
                    val handle = getOrCreateHandle(message.documentId)
                    handle.receiveMessage(message)
                }
            }

            log.i { "Connecting network adapters" }
            network.connect(localPeerId)
        }
    }

    private suspend fun addPeer(peerId: PeerId) {
        log.d { "Add peer $peerId" }
        allPeers.update { it + peerId }
        handles.value.forEach { (documentId, handle) ->
            // TODO check the sharePolicy
            handle.beginSynchronization(setOf(peerId))
        }
    }

    private fun removePeer(peerId: PeerId) {
        log.d { "Remove peer $peerId" }
        allPeers.update { it - peerId }
        handles.value.forEach { (_, handle) ->
            handle.endSynchronization(peerId)
        }
    }

    private fun createHandle(documentId: DocumentId): DocumentHandle {
        val handle = DocumentHandle(this, documentId)
        handles.update {
            require(documentId !in it)
            it + (documentId to handle)
        }
        return handle
    }

    private fun getOrCreateHandle(documentId: DocumentId): DocumentHandle {
        val updated = handles.updateAndGet {
            if (documentId in it) it
            else it + (documentId to DocumentHandle(this, documentId))
        }
        return updated[documentId]!!
    }

    private fun setupDocument(handle: DocumentHandle, factory: suspend () -> Document?) {
        coroutineScope.launch {
            handle.setDocument(factory())
        }
    }

    internal suspend fun sendSynchronizationMessage(targetId: PeerId, documentId: DocumentId, data: ByteArray) {
        network.send(PeerMessage.Synchronization(localPeerId, targetId, documentId, data))
    }

    internal suspend fun sendEphemeralMessage(targetId: PeerId, documentId: DocumentId, data: ByteArray) {
        network.send(PeerMessage.Ephemeral(localPeerId, targetId, documentId, data))
    }

    public fun create(factory: () -> Document = { Document() }): DocumentHandle {
        val documentId = DocumentId.randomDocumentId()
        val handle = createHandle(documentId)
        setupDocument(handle, factory)
        return handle
    }

    public fun find(documentId: DocumentId): DocumentHandle {
        val handle = getOrCreateHandle(documentId)
        setupDocument(handle) { storage?.get(documentId)?.let { Document.load(it) } }
        return handle
    }

    public fun findOrCreate(documentId: DocumentId, factory: () -> Document): DocumentHandle {
        val handle = getOrCreateHandle(documentId)
        setupDocument(handle) { storage?.get(documentId)?.let { Document.load(it) } ?: factory() }
        return handle
    }

    public fun delete(documentId: DocumentId) {
        TODO()
    }
}
