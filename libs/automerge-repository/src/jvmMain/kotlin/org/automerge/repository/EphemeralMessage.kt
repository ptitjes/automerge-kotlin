package org.automerge.repository

public data class EphemeralMessage(
    val senderId: PeerId,
    val data: ByteArray,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EphemeralMessage

        if (senderId != other.senderId) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = senderId.hashCode()
        result = 31 * result + data.contentHashCode()
        return result
    }

    @OptIn(ExperimentalStdlibApi::class)
    override fun toString(): String {
        return "EphemeralMessage(senderId=$senderId, data=${data.toHexString()})"
    }
}
