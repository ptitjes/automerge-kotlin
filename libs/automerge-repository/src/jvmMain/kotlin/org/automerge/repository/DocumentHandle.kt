package org.automerge.repository

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.getAndUpdate
import kotlinx.atomicfu.updateAndGet
import kotlinx.coroutines.flow.*
import org.automerge.Document
import org.automerge.Patch
import org.automerge.SyncState
import org.automerge.Transaction
import org.automerge.repository.network.PeerMessage

public class DocumentHandle internal constructor(
    private val repository: DocumentRepository,
    public val documentId: DocumentId,
) {
    internal suspend fun setDocument(document: Document?) {
        _statuses.emit(
            if (document == null) Status.Unavailable
            else Status.Ready(document)
        )
    }

    private val syncStates = atomic(mapOf<PeerId, SyncState>())

    private fun hasPeer(peer: PeerId): Boolean = peer in syncStates.value

    private fun getOrCreateSyncState(peer: PeerId): SyncState {
        val updated = syncStates.updateAndGet {
            if (peer in it) it
            else it + (peer to SyncState())
        }
        return updated[peer]!!
    }

    internal suspend fun beginSynchronization(peers: Set<PeerId>) {
        val newPeers = peers.filter { !hasPeer(it) }

        awaitStatus { it !is Status.Idle }

        val document = when (val status = statuses.value) {
            is Status.Ready -> status.document
            is Status.Unavailable -> Document()
            else -> error("Invalid status")
        }

        newPeers.forEach { peer ->
            val syncState = getOrCreateSyncState(peer)
            sendSynchronizationMessage(document, peer, syncState)
        }
    }

    private suspend fun sendSynchronizationMessage(document: Document, peer: PeerId, syncState: SyncState) {
        val message = document.generateSyncMessage(syncState).orElse(null)
        if (message != null) repository.sendSynchronizationMessage(peer, documentId, message)
    }

    internal fun endSynchronization(peer: PeerId) {
        val syncState = syncStates.getAndUpdate { it - peer }[peer]
        syncState?.free()
    }

    internal suspend fun receiveMessage(message: PeerMessage) {
        require(message.documentId == documentId)

        when (message) {
            is PeerMessage.Synchronization -> receiveSynchronizationMessage(message)
            is PeerMessage.Ephemeral -> receiveEphemeralMessage(message)
        }
    }

    private suspend fun receiveSynchronizationMessage(message: PeerMessage.Synchronization) {
        awaitStatus { it !is Status.Idle }

        val document = when (val status = statuses.value) {
            is Status.Ready -> status.document
            is Status.Unavailable -> Document()
            else -> error("Invalid status")
        }

        val syncState = getOrCreateSyncState(message.senderId)
        val patches = document.receiveSyncMessageWithPatches(syncState, message.data)
        _changes.emit(patches)
    }

    private val _statuses = MutableStateFlow<Status>(Status.Idle)
    public val statuses: StateFlow<Status> get() = _statuses

    private suspend fun awaitStatus(predicate: (Status) -> Boolean) {
        statuses.filter { predicate(it) }.first()
    }

    public suspend fun change(changeFunction: Transaction.() -> Unit) {
        val status = statuses.value

        if (status !is Status.Ready) error("Document is not ready")

        val patches = status.document.withPatches { log ->
            val transaction = startTransaction(log)
            transaction.changeFunction()
            val changeHash = transaction.commit().orElse(null)
            if (changeHash != null) {
                syncStates.value.forEach { (peer, syncState) ->
                    sendSynchronizationMessage(this, peer, syncState)
                }
            }
        }

        _changes.emit(patches)
    }

    private val _changes = MutableSharedFlow<List<Patch>>()
    public val changes: SharedFlow<List<Patch>> get() = _changes

    public suspend fun broadcastEphemeralMessage(data: ByteArray) {
        syncStates.value.keys.forEach { peer ->
            repository.sendEphemeralMessage(peer, documentId, data)
        }
    }

    private suspend fun receiveEphemeralMessage(message: PeerMessage.Ephemeral) {
        _ephemeralMessages.emit(EphemeralMessage(message.senderId, message.data))
    }

    private val _ephemeralMessages = MutableSharedFlow<EphemeralMessage>()
    public val ephemeralMessages: SharedFlow<EphemeralMessage> get() = _ephemeralMessages

    public sealed interface Status {
        public data object Idle : Status
        public data object Unavailable : Status
        public data class Ready(val document: Document) : Status
        public data object Deleted : Status
        // We keep the local document when deleted
        // Just do not propagate the changes anymore
    }
}
