package org.automerge.repository.network

import org.automerge.repository.PeerId

public sealed interface PeerEvent {
    public val peerId: PeerId

    public data class Join(override val peerId: PeerId) : PeerEvent
    public data class Welcome(override val peerId: PeerId) : PeerEvent
}
