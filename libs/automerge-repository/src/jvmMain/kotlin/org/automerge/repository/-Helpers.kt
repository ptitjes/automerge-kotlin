package org.automerge.repository

import org.automerge.Document
import org.automerge.Patch
import org.automerge.PatchLog
import org.automerge.SyncState

internal inline fun Document.withPatches(block: Document.(log: PatchLog) -> Unit): List<Patch> {
    val log = PatchLog()
    try {
        block(log)
        return makePatches(log)
    } finally {
        log.free()
    }
}

internal fun Document.mergeWithPatches(other: Document): List<Patch> = withPatches { log -> merge(other, log) }

internal fun Document.applyEncodedChangesWithPatches(changes: ByteArray): List<Patch> =
    withPatches { log -> applyEncodedChanges(changes, log) }

internal fun Document.receiveSyncMessageWithPatches(syncState: SyncState, message: ByteArray): List<Patch> =
    withPatches { log -> receiveSyncMessage(syncState, log, message) }
