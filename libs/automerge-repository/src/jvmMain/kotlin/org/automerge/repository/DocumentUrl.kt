package org.automerge.repository

private const val AUTOMERGE_URL_PREFIX = "automerge:"

public fun String.automergeUrlToDocumentId(): DocumentId {
    require(startsWith(AUTOMERGE_URL_PREFIX)) { "Not a valid Automerge URL" }
    return removePrefix(AUTOMERGE_URL_PREFIX).toDocumentId()
}

public fun DocumentId.toAutomergeUrl(): String = "$AUTOMERGE_URL_PREFIX${this}"
