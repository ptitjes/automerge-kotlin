package org.automerge.repository.network

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.update
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import org.automerge.repository.PeerId

internal class NetworkSubsystem(
    private val adapters: List<NetworkAdapter>,
) {
    private val adapterByPeer = atomic(mapOf<PeerId, NetworkAdapter>())

    suspend fun connect(peerId: PeerId) {
        adapters.forEach { adapter -> adapter.connect(peerId) }
    }

    val peerEvents: Flow<PeerEvent> = adapters.map { adapter ->
        adapter.peerEvents.onEach { event ->
            when {
                (event is PeerEvent.Join || event is PeerEvent.Welcome) && event.peerId !in adapterByPeer.value ->
                    adapterByPeer.update { it + (event.peerId to adapter) }

//                event is PeerEvent.Disconnected ->
//                    adapterByPeer.update { it - event.peerId }
            }
        }
    }.merge()

    suspend fun send(message: PeerMessage) {
        val adapter = adapterByPeer.value[message.targetId] ?: error("No adapter for ${message.targetId}")
        adapter.send(message)
    }

    val messages: Flow<PeerMessage> = adapters.map { adapter -> adapter.peerMessages }.merge()
}
