package org.automerge.repository.storage

import org.automerge.repository.DocumentId

internal class StorageSubsystem(
    private val adapter: StorageAdapter,
) {
    suspend fun get(documentId: DocumentId): ByteArray? {
        return null
    }

    suspend fun append(documentId: DocumentId, changes: ByteArray) {
        TODO()
    }

    suspend fun compact(documentId: DocumentId) {
        TODO()
    }
}
