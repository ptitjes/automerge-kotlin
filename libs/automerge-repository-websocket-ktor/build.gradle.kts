plugins {
    id("automerge-library")
}

kotlin {
    jvm()

    sourceSets {
        commonMain {
            dependencies {
                implementation(libs.kotlinx.atomicfu)
                implementation(libs.kotlinx.coroutines.core)

                implementation(project(":libs:automerge-repository"))

                implementation(libs.kermit)

                implementation(libs.bundles.ktor.server)
                implementation(libs.bundles.ktor.client)
                implementation(libs.ktor.serialization.cbor)
            }
        }

        jvmMain {
            dependencies {
                implementation(libs.automerge.java)
            }
        }

        jvmTest {
            dependencies {
                implementation(kotlin("test"))
            }
        }
    }
}
