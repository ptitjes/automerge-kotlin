package org.automerge.repository.websocket.ktor

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import org.automerge.repository.PeerId
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalSerializationApi::class)
class WebSocketServerMessageTests {

    private val format = Cbor

    @Test
    fun peerMessageSerialization() {
        testSerialization(
            WebSocketServerMessage.Peer(
                senderId = PeerId("me"),
                selectedProtocolVersion = "1",
                targetId = PeerId("other"),
            )
        )
    }

    @Test
    fun errorMessageSerialization() {
        testSerialization(
            WebSocketServerMessage.Error(
                senderId = PeerId("me"),
                message = "failure",
                targetId = PeerId("other"),
            )
        )
    }

    private fun testSerialization(original: WebSocketServerMessage) {
        val serialized = format.encodeToByteArray(original)
        val deserialized = format.decodeFromByteArray<WebSocketServerMessage>(serialized)

        assertEquals(original, deserialized)
    }
}
