package org.automerge.repository.websocket.ktor.serializers

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.automerge.repository.PeerId

internal class PeerIdSerializer : KSerializer<PeerId> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("PeerId", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): PeerId = PeerId(decoder.decodeString())
    override fun serialize(encoder: Encoder, value: PeerId) = encoder.encodeString(value.id)
}