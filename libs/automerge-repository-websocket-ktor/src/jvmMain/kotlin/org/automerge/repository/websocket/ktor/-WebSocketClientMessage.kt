@file:UseSerializers(
    PeerIdSerializer::class,
    DocumentIdSerializer::class,
)

package org.automerge.repository.websocket.ktor

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.automerge.repository.PeerId
import org.automerge.repository.network.PeerMessage
import org.automerge.repository.websocket.ktor.serializers.DocumentIdSerializer
import org.automerge.repository.websocket.ktor.serializers.PeerIdSerializer

@Serializable
internal sealed interface WebSocketClientMessage {

    @Serializable
    @SerialName("join")
    data class Join(
        val senderId: PeerId,
        val supportedProtocolVersions: List<String>,
    ) : WebSocketClientMessage

    @Serializable
    @SerialName("leave")
    data class Leave(
        val senderId: PeerId,
    ) : WebSocketClientMessage

    data class Message(
        val message: PeerMessage,
    ) : WebSocketClientMessage
}
