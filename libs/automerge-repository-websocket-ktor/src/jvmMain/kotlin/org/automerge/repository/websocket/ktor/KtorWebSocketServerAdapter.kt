package org.automerge.repository.websocket.ktor

import kotlinx.coroutines.flow.Flow
import org.automerge.repository.PeerId
import org.automerge.repository.network.NetworkAdapter
import org.automerge.repository.network.PeerMessage
import org.automerge.repository.network.PeerEvent

public class KtorWebSocketServerAdapter(

) : NetworkAdapter {
    override suspend fun connect(peerId: PeerId) {
        TODO("Not yet implemented")
    }

    override val peerEvents: Flow<PeerEvent>
        get() = TODO("Not yet implemented")

    override suspend fun send(message: PeerMessage) {
        TODO("Not yet implemented")
    }

    override val peerMessages: Flow<PeerMessage>
        get() = TODO("Not yet implemented")
}
