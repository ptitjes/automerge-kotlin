package org.automerge.repository.websocket.ktor

import co.touchlab.kermit.Logger
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.websocket.*
import io.ktor.serialization.kotlinx.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.cbor.Cbor
import org.automerge.repository.PeerId
import org.automerge.repository.network.NetworkAdapter
import org.automerge.repository.network.PeerEvent
import org.automerge.repository.network.PeerMessage

private const val SUPPORTED_PROTOCOL_VERSION = "1"

public class KtorWebSocketClientAdapter(
    private val clientEngine: HttpClientEngine,
    private val url: String,
) : NetworkAdapter {

    private val log = Logger.withTag("KtorWebSocketClientAdapter-$url")

    @OptIn(ExperimentalSerializationApi::class)
    private val client = HttpClient(clientEngine) {
        install(WebSockets) {
            contentConverter = KotlinxWebsocketSerializationConverter(Cbor)
        }
    }

    private val _peerEvents = MutableSharedFlow<PeerEvent>()
    private val _peerMessages = MutableSharedFlow<PeerMessage>()

    override suspend fun connect(peerId: PeerId) {
        client.webSocket(url) {
            launch {
                while (true) {
                    when (val message = receiveDeserialized<WebSocketServerMessage>()) {
                        is WebSocketServerMessage.Peer -> _peerEvents.emit(
                            PeerEvent.Join(peerId = message.senderId)
                        )

                        is WebSocketServerMessage.Error -> log.e { message.message }

                        else -> {}
                    }
                }
            }

            sendSerialized(
                WebSocketClientMessage.Join(
                    senderId = peerId, supportedProtocolVersions = listOf(SUPPORTED_PROTOCOL_VERSION)
                )
            )
        }
    }

    override val peerEvents: Flow<PeerEvent> = _peerEvents.asSharedFlow()

    override suspend fun send(message: PeerMessage) {
        TODO("Not yet implemented")
    }

    override val peerMessages: Flow<PeerMessage> = _peerMessages.asSharedFlow()
}
