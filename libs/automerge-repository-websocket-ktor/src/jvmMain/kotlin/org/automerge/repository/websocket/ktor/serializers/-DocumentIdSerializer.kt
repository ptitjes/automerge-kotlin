package org.automerge.repository.websocket.ktor.serializers

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.automerge.repository.DocumentId
import org.automerge.repository.toDocumentId

internal class DocumentIdSerializer : KSerializer<DocumentId> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("DocumentId", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): DocumentId = decoder.decodeString().toDocumentId()
    override fun serialize(encoder: Encoder, value: DocumentId) = encoder.encodeString(value.toString())
}
