@file:UseSerializers(
    PeerIdSerializer::class,
    DocumentIdSerializer::class,
)

package org.automerge.repository.websocket.ktor

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.automerge.repository.DocumentId
import org.automerge.repository.PeerId
import org.automerge.repository.websocket.ktor.serializers.DocumentIdSerializer
import org.automerge.repository.websocket.ktor.serializers.PeerIdSerializer

@Serializable
internal sealed interface WebSocketServerMessage {
    val senderId: PeerId

    @Serializable
    @SerialName("peer")
    data class Peer(
        override val senderId: PeerId,
        val selectedProtocolVersion: String,
        val targetId: PeerId,
    ) : WebSocketServerMessage

    @Serializable
    @SerialName("error")
    data class Error(
        override val senderId: PeerId,
        val message: String,
        val targetId: PeerId,
    ) : WebSocketServerMessage

    @Serializable
    @SerialName("sync")
    data class Synchronization(
        override val senderId: PeerId,
        val targetId: PeerId,
        val documentId: DocumentId,
        val data: ByteArray,
    ) : WebSocketServerMessage {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Synchronization

            if (senderId != other.senderId) return false
            if (targetId != other.targetId) return false
            if (documentId != other.documentId) return false
            if (!data.contentEquals(other.data)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = senderId.hashCode()
            result = 31 * result + targetId.hashCode()
            result = 31 * result + documentId.hashCode()
            result = 31 * result + data.contentHashCode()
            return result
        }
    }

    @Serializable
    @SerialName("ephemeral")
    data class Ephemeral(
        override val senderId: PeerId,
        val targetId: PeerId,
        val documentId: DocumentId,
        val data: ByteArray,
    ) : WebSocketServerMessage {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Ephemeral

            if (senderId != other.senderId) return false
            if (targetId != other.targetId) return false
            if (documentId != other.documentId) return false
            if (!data.contentEquals(other.data)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = senderId.hashCode()
            result = 31 * result + targetId.hashCode()
            result = 31 * result + documentId.hashCode()
            result = 31 * result + data.contentHashCode()
            return result
        }
    }
}
