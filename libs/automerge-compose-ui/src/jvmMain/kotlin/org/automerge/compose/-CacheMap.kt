package org.automerge.compose

import java.lang.ref.ReferenceQueue
import java.lang.ref.WeakReference

internal class CacheMap<K, V> : AbstractMutableMap<K, V>() {
    private val cache = mutableMapOf<K, WeakEntry>()
    private val referenceQueue = ReferenceQueue<V>()

    private fun newEntry(key: K, value: V) = WeakEntry(key, value)

    private fun cleanup() {
        do {
            val reference = referenceQueue.poll() as CacheMap<*, *>.WeakEntry?
            if (reference != null) cache.remove(reference.key)
        } while (reference != null)
    }

    private inline fun <T> withCleanup(block: () -> T): T {
        cleanup()
        return block()
    }

    private inner class WeakEntry(
        override val key: K,
        value: V,
    ) : WeakReference<V>(value, referenceQueue), MutableMap.MutableEntry<K, V> {
        override val value: V get() = get() ?: error("Invalid state")

        override fun setValue(newValue: V): V {
            TODO("Not yet implemented")
        }
    }

    override val size: Int get() = withCleanup { cache.size }

    override fun isEmpty(): Boolean = withCleanup { cache.isEmpty() }

    override fun containsKey(key: K): Boolean = withCleanup { cache.containsKey(key) }

    override operator fun get(key: K): V? = withCleanup { cache[key]?.get() }

    override fun put(key: K, value: V): V? = withCleanup { cache.put(key, newEntry(key, value))?.get() }

    override fun remove(key: K): V? = withCleanup { cache.remove(key)?.get() }

    override fun putAll(from: Map<out K, V>) = withCleanup {
        from.forEach { (key, value) -> put(key, value) }
    }

    override fun clear(): Unit = cache.clear()

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>> get() = EntrySet()

    inner class EntrySet : AbstractMutableSet<MutableMap.MutableEntry<K, V>>() {
        override val size: Int get() = this@CacheMap.size

        override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, V>> =
            withCleanup { EntryIterator() }

        override fun add(element: MutableMap.MutableEntry<K, V>): Boolean =
            put(element.key, element.value) != element.value
    }

    inner class EntryIterator : AbstractIterator<MutableMap.MutableEntry<K, V>>(),
        MutableIterator<MutableMap.MutableEntry<K, V>> {

        private val entries = this@CacheMap.cache.values.iterator()

        override fun computeNext() {
            if (entries.hasNext()) {
                setNext(entries.next())
            } else {
                done()
            }
        }

        override fun remove() {
            throw UnsupportedOperationException()
        }
    }
}