package org.automerge.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import org.automerge.repository.DocumentRepository
import org.automerge.repository.network.NetworkAdapter
import org.automerge.repository.newDocumentRepository
import org.automerge.repository.storage.StorageAdapter

@Composable
public fun rememberDocumentRepository(
    networkAdapters: List<NetworkAdapter>,
    storageAdapter: StorageAdapter? = null,
): DocumentRepository {
    val coroutineScope = rememberCoroutineScope()
    return remember { coroutineScope.newDocumentRepository(networkAdapters, storageAdapter) }
}
