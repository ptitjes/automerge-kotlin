package org.automerge.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import kotlinx.coroutines.CoroutineScope
import org.automerge.ObjectId
import org.automerge.repository.DocumentHandle

@Composable
public fun <T : AmNodeState> rememberDocumentState(
    handle: DocumentHandle,
    factory: () -> T,
): AmDocumentState<T> {
    val coroutineScope = rememberCoroutineScope()
    return remember { AmDocumentState(coroutineScope, handle) { factory() } }
}

public class AmDocumentState<T : AmNodeState>(
    internal val coroutineScope: CoroutineScope,
    internal val handle: DocumentHandle,
    factory: () -> T,
) {
//    private val objectStateCache = CacheMap<ObjectId, AmNodeState>()

    public val root: T = factory()

    init {
        root.setup(this, ObjectId.ROOT)
    }
}
