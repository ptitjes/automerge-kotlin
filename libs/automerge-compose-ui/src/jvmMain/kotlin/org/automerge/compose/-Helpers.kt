package org.automerge.compose

import org.automerge.*
import java.util.*

internal fun Read.get(objectId: ObjectId, prop: Prop): AmValue? = when (prop) {
    is Prop.Key -> get(objectId, prop.key).unwrap()
    is Prop.Index -> get(objectId, prop.index.toInt()).unwrap()
    else -> error("Invalid prop") // Prop should be a sealed class
}

internal fun Transaction.set(objectId: ObjectId, prop: Prop, value: NewValue) {
    when (prop) {
        is Prop.Key -> set(objectId, prop.key, value)
        is Prop.Index -> set(objectId, prop.index, value)
        else -> error("Invalid prop") // Prop should be a sealed class
    }
}

internal fun Transaction.increment(objectId: ObjectId, prop: Prop, amount: Long) {
    when (prop) {
        is Prop.Key -> increment(objectId, prop.key, amount)
        is Prop.Index -> increment(objectId, prop.index, amount)
        else -> error("Invalid prop") // Prop should be a sealed class
    }
}

internal fun <T> Optional<T>.unwrap(): T? = orElse(null)
