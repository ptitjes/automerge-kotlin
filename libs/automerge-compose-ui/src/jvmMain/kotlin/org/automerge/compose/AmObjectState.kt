package org.automerge.compose

import androidx.compose.runtime.MutableState
import kotlinx.coroutines.launch
import kotlinx.datetime.Instant
import org.automerge.AmValue
import org.automerge.PatchAction
import org.automerge.Prop
import org.automerge.Transaction
import org.automerge.repository.DocumentHandle

public open class AmObjectState : AmNodeState() {

    override fun setupChildren() {
        super.setupChildren()

        childNodeStates.forEach { (key, child) ->
            val amValue = document().get(objectId, key).unwrap()
            val objectId = (amValue as? AmValue.Map)?.id ?: error("Invalid value type")
            child.setup(documentState, objectId)
        }
    }

    private val propertyStates = mutableMapOf<String, AmScalarValueState<*>>()
    private val childNodeStates = mutableMapOf<String, AmNodeState>()

    override fun applyPatchAction(action: PatchAction) {
        when (action) {
            is PatchAction.PutMap -> propertyStates[action.key]?.applyPatchAction(action)
            is PatchAction.DeleteMap -> propertyStates[action.key]?.applyPatchAction(action)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Any> property(
        key: String,
        valueHandler: AmScalarValueHandler<T>,
    ): MutableState<T> {
        return propertyStates.getOrPut(key) {
            AmScalarValueState(
                source = ::document,
                objectId = ::objectId,
                prop = Prop.Key(key),
                onChange = ::onPropertyChange,
                valueHandler = valueHandler,
            )
        } as MutableState<T>
    }

    private fun onPropertyChange(applier: Transaction.() -> Unit) {
        coroutineScope.launch {
            handle.change { applier() }
        }
    }

    private fun document() = (handle.statuses.value as? DocumentHandle.Status.Ready)?.document ?: error("Invalid state")

    protected fun booleanProperty(key: String): MutableState<Boolean> = property(key, AmBooleanHandler)
    protected fun uintProperty(key: String): MutableState<ULong> = property(key, AmUIntHandler)
    protected fun intProperty(key: String): MutableState<Long> = property(key, AmIntHandler)
    protected fun bytesProperty(key: String): MutableState<ByteArray> = property(key, AmBytesHandler)
    protected fun strProperty(key: String): MutableState<String> = property(key, AmStrHandler)
    protected fun f64Property(key: String): MutableState<Double> = property(key, AmF64Handler)
    protected fun timestampProperty(key: String): MutableState<Instant> = property(key, AmTimestampHandler)

    @Suppress("UNCHECKED_CAST")
    protected fun <T : AmObjectState> objectProperty(key: String, factory: () -> T): T {
        return childNodeStates.getOrPut(key) { factory() } as T
    }
}
