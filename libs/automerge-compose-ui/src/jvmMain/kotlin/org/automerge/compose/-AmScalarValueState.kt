package org.automerge.compose

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import org.automerge.*

internal class AmScalarValueState<T : Any>(
    source: () -> Read,
    private val objectId: () -> ObjectId,
    private val prop: Prop,
    private val onChange: (applier: Transaction.() -> Unit) -> Unit,
    private val valueHandler: AmScalarValueHandler<T>,
) : MutableState<T> {

    private val state by lazy {
        mutableStateOf(
            with(valueHandler) { source().get(objectId(), prop)?.unwrapValue() } ?: error("No value")
        )
    }

    override var value: T
        get() = state.value
        set(value) {
            state.value = value
            onChange {
                set(objectId(), prop, with(valueHandler) { value.wrapValue() })
            }
        }

    override fun component1(): T = value
    override fun component2(): (T) -> Unit = { value = it }

    fun applyPatchAction(action: PatchAction) {
        state.value = with(valueHandler) { action.extractPatchedValue() }
    }
}
