package org.automerge.compose

import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toKotlinInstant
import org.automerge.AmValue
import org.automerge.NewValue
import org.automerge.PatchAction
import java.util.*

internal sealed interface AmScalarValueHandler<T> {
    fun AmValue.unwrapValue(): T
    fun T.wrapValue(): NewValue
    fun PatchAction.extractPatchedValue(): T
}

internal sealed class AmSimpleScalarHandler<T> : AmScalarValueHandler<T> {
    final override fun PatchAction.extractPatchedValue(): T = when (this) {
        is PatchAction.PutMap -> this.value.unwrapValue()
        is PatchAction.PutList -> this.value.unwrapValue()
        is PatchAction.DeleteMap -> TODO()
        is PatchAction.DeleteList -> TODO()
        else -> error("Invalid patch action")
    }
}

internal object AmBooleanHandler : AmSimpleScalarHandler<Boolean>() {
    override fun AmValue.unwrapValue(): Boolean {
        val amValue = this as? AmValue.Bool ?: error("Invalid value type")
        return amValue.value
    }

    override fun Boolean.wrapValue(): NewValue = NewValue.bool(this)
}

internal object AmIntHandler : AmSimpleScalarHandler<Long>() {
    override fun AmValue.unwrapValue(): Long {
        val amValue = this as? AmValue.Int ?: error("Invalid value type")
        return amValue.value
    }

    override fun Long.wrapValue(): NewValue = NewValue.integer(this)
}

internal object AmUIntHandler : AmSimpleScalarHandler<ULong>() {
    override fun AmValue.unwrapValue(): ULong {
        val amValue = this as? AmValue.UInt ?: error("Invalid value type")
        return amValue.value.toULong()
    }

    override fun ULong.wrapValue(): NewValue = NewValue.uint(this.toLong())
}

internal object AmBytesHandler : AmSimpleScalarHandler<ByteArray>() {
    override fun AmValue.unwrapValue(): ByteArray {
        val amValue = this as? AmValue.Bytes ?: error("Invalid value type")
        return amValue.value
    }

    override fun ByteArray.wrapValue(): NewValue = NewValue.bytes(this)
}

internal object AmStrHandler : AmSimpleScalarHandler<String>() {
    override fun AmValue.unwrapValue(): String {
        val amValue = this as? AmValue.Str ?: error("Invalid value type")
        return amValue.value
    }

    override fun String.wrapValue(): NewValue = NewValue.str(this)
}

internal object AmF64Handler : AmSimpleScalarHandler<Double>() {
    override fun AmValue.unwrapValue(): Double {
        val amValue = this as? AmValue.F64 ?: error("Invalid value type")
        return amValue.value
    }

    override fun Double.wrapValue(): NewValue = NewValue.f64(this)
}

internal object AmTimestampHandler : AmSimpleScalarHandler<Instant>() {
    override fun AmValue.unwrapValue(): Instant {
        val amValue = this as? AmValue.Timestamp ?: error("Invalid value type")
        return amValue.value.toInstant().toKotlinInstant()
    }

    override fun Instant.wrapValue(): NewValue = NewValue.timestamp(Date.from(this.toJavaInstant()))
}