package org.automerge.compose

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.automerge.ObjectId
import org.automerge.PatchAction
import org.automerge.repository.DocumentHandle

public sealed class AmNodeState {
    internal lateinit var documentState: AmDocumentState<*>
    internal lateinit var objectId: ObjectId

    internal fun setup(documentState: AmDocumentState<*>, objectId: ObjectId) {
        this.documentState = documentState
        this.objectId = objectId

        listenToChanges()
        setupChildren()
    }

    internal val coroutineScope: CoroutineScope get() = documentState.coroutineScope
    internal val handle: DocumentHandle get() = documentState.handle

    private fun listenToChanges() {
        coroutineScope.launch {
            handle.changes.collect { patches ->
                for (patch in patches) {
                    if (patch.obj == objectId) applyPatchAction(patch.action)
                }
            }
        }
    }

    internal open fun setupChildren() {}

    internal abstract fun applyPatchAction(action: PatchAction)
}
