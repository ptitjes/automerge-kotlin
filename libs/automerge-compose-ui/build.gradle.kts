plugins {
    id("automerge-library")
    id("automerge-compose")
}

kotlin {
    jvm()

    sourceSets {
        jvmMain {
            dependencies {
                implementation(libs.automerge.java)
                implementation(project(":libs:automerge-repository"))
                implementation(compose.desktop.common)
                implementation(libs.kotlinx.datetime)
            }
        }
    }
}
