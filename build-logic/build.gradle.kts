plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.kotlin.gradle.plugin)
    implementation(libs.kotlinx.atomicfu.plugin)
    implementation(libs.kotlinx.serialization.plugin)
    implementation(libs.compose.plugin)
}
