
val projectVersion: String = libs.versions.project.get()

subprojects {
    group = "org.automerge"
    version = projectVersion
}

subprojects.filter { it.projectDir.startsWith("libs") }.forEach {

}
